# [Learn JMETER from Scratch on Live Apps - Performance Testing](https://www.udemy.com/course/learn-jmeter-from-scratch-performance-load-testing-tool/)

## Recordings

### 02-07 Recording in Chrome

- Add to Chrome 'BlazeMEter - The Continous Testing Platform' from the chrome web store.
- You should see the BlazeMeter icon in the Chrome browser
- click on icon
- BlazeMeter Chrome tool opens with Welcome! page
- Note: BlazeMeter records .jmx files
- Open again BlazeMEter tool and click on big red Start recording button
- OPen blazedemo.com
- Select Boston and London destinations
- select flight number 234
- Now save recording as .jmx
