# Udemy Learning

## Overview

YouTube: https://www.youtube.com/watch?v=cv7KqxaLZd8
Test website: http://puppies.herokuapp.com
Login: http://puppies.herokuapp.com/login, user: admin, password: password

Install and run JMeter:

- to install just unzip the jmeter in some dir, then add 'bin' subfolder to the PATH
- run it on Windows using lmeter.bat script
