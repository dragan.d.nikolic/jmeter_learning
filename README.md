# jmeter_learning

## install jmeter

- Make sure Java 8 or newer is installed.
- Download binaries from http://jmeter.apache.org/download_jmeter.cgi.

### windows

Unzip the content of the archive in a folder and run jmeter.bat from bin folder.

### mac

Untar the content of the archive in a folder and run jmeter.sh from bin folder.
