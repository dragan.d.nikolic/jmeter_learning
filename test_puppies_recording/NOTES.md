Recording with Firefox 

- in jMeter create new Recording template (File/Templates.../Recording)
- configure Firefox Network Proxy to (Manual proxy configuration / HTTP Proxy: localhost, Port: 8888)
- in jMeter start HTTP Test Script Recorder (just clikc on Start button)
  note: without this Firefox willnopt be able to browse
- navigate in Firefox to the home page http://puppies.herokuapp.com
  note: jMeter creates coresponding HTTP request in  Thread Group/Recording Controller
- navigate in Firefox to the details page for a puppy
  note: jMeter creates coresponding HTTP request in  Thread Group/Recording Controller
- stop recording
- in jMeter Thread Group specify some values for Number of Threads, Rump up period and Loop Count
- select View Results Tree then execute the jMeter test
  note: you should see exection of the requests based on the values entered (ie. within Rump up time
  there should be 'Number o Threads' X 'Loop Count' requests)


Recording with Chrome

- 